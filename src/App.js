import React from 'react';
import { CircularProgressbar } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';

import './App.css';

function App() {
  const [gasVal, setGasVal] = React.useState([]);

  React.useEffect(() => {
    async function getMq2() {
      try {
        const data = await fetch(
          ' https://api.thingspeak.com/channels/2166949/fields/1.json?api_key=X9471OO3T7HIVXZ7&results=2',
        ).then((res) => res.json());

        setGasVal(data.feeds[1].field1);
      } catch (error) {
        console.log(error);
      }
    }

    getMq2();
  });

  return (
    <div className="wrapper">
      <div className="content">
        <h2>Уровень газа</h2>

        <div className="circle-container">
          <CircularProgressbar value={(gasVal / 100).toFixed(2)} />
          <span className="circle-val">{(gasVal / 100).toFixed(2)} ppm</span>
        </div>
        <div style={{ margin: '2.5rem 0 0 0' }}>
          {gasVal > 400 ? (
            <p style={{ color: 'red', fontSize: '20px' }}>Превышен уровень углекислого газа!!!</p>
          ) : (
            <p style={{ color: 'green', fontSize: '20px' }}>уровень углекислого газа в норме</p>
          )}
        </div>
      </div>
    </div>
  );
}

export default App;
